#include <dune/opcounter/opcounter.hh>
#include <dune/opcounter/vectorclass.hh>
#include <vectorclass/vectorclass.h>
#include <vectorclass/vectormath_common.h>
#include <vectorclass/vectorf128.h>
#include <iostream>

using num_type = OpCounter::OpCounter<double>;
using vec_type = OpCounter::impl::OpCounterVector<double, 4>;
using bool_vec_type = _vcl::Vec4db;

/* These functions have been copied from the vectorclass code and modified to
 * function with OpCounters. The aliases above might not immediately work for
 * every type. The modifications are not exactly clean.
 */

// Helper functions, used internally:

// This function calculates pow(2,n) where n must be an integer. Does not check for overflow or underflow
static inline vec_type vm_pow2n (vec_type const & n) {
    const num_type pow2_52 = 4503599627370496.0;   // 2^52
    const num_type bias = 1023.0;                  // bias in exponent
    vec_type a1 = n + (bias + pow2_52);             // put n + bias in least significant bits
    _vcl::Vec4d a2 = OpCounter::impl::to_original(a1);
    _vcl::Vec4q b = _vcl::reinterpret_i(a2);        // bit-cast to integer
    _vcl::Vec4q c = b << 52;                       // shift left 52 places to get into exponent field
    _vcl::Vec4d d1 = _vcl::reinterpret_d(c);           // bit-cast back to double
    vec_type d2 = OpCounter::impl::to_opcounted<double, 4>(d1);
    return d2;
}
/*
static inline Vec4f vm_pow2n (Vec4f const & n) {
    const num_type pow2_23 =  8388608.0;            // 2^23
    const num_type bias = 127.0;                    // bias in exponent
    Vec4f a = n + (bias + pow2_23);              // put n + bias in least significant bits
    Vec4i b = reinterpret_i(a);                  // bit-cast to integer
    Vec4i c = b << 23;                           // shift left 23 places to get into exponent field
    Vec4f d = reinterpret_f(c);                  // bit-cast back to float
    return d;
}*/

// Template for exp function, double precision
// The limit of abs(x) is defined by max_x below
// This function does not produce denormals
// Template parameters:
// VTYPE:  double vector type
// BVTYPE: boolean vector type
// M1: 0 for exp, 1 for expm1
// BA: 0 for exp, 1 for 0.5*exp, 2 for pow(2,x), 10 for pow(10,x)

// Taylor expansion
template<class VTYPE, class BVTYPE, int M1, int BA> 
static inline VTYPE opc_experiment_exp_d(VTYPE const & initial_x) {    

    // Taylor coefficients, 1/n!
    // Not using minimax approximation because we prioritize precision close to x = 0
    const num_type p2  = 1./2.;
    const num_type p3  = 1./6.;
    const num_type p4  = 1./24.;
    const num_type p5  = 1./120.; 
    const num_type p6  = 1./720.; 
    const num_type p7  = 1./5040.; 
    const num_type p8  = 1./40320.; 
    const num_type p9  = 1./362880.; 
    const num_type p10 = 1./3628800.; 
    const num_type p11 = 1./39916800.; 
    const num_type p12 = 1./479001600.; 
    const num_type p13 = 1./6227020800.; 

    // maximum abs(x), value depends on BA, defined below
    // The lower limit of x is slightly more restrictive than the upper limit.
    // We are specifying the lower limit, except for BA = 1 because it is not used for negative x
    num_type max_x;

    // data vectors
    VTYPE  x, r, z, n2;
    BVTYPE inrange;                              // boolean vector

    if (BA <= 1) { // exp(x)
        max_x = BA == 0 ? 708.39 : 709.7; // lower limit for 0.5*exp(x) is -707.6, but we are using 0.5*exp(x) only for positive x in hyperbolic functions
        const num_type ln2d_hi = 0.693145751953125;
        const num_type ln2d_lo = 1.42860682030941723212E-6;
        x  = initial_x;
        r  = round(initial_x*num_type(VM_LOG2E));
        // subtraction in two steps for higher precision
        x = nmul_add(r, ln2d_hi, x);             //  x -= r * ln2d_hi;
        x = nmul_add(r, ln2d_lo, x);             //  x -= r * ln2d_lo;
    }
    else if (BA == 2) { // pow(2,x)
        max_x = 1022.0;
        r  = round(initial_x);
        x  = initial_x - r;
        x  = x*num_type(VM_LN2);
    }
    else if (BA == 10) { // pow(10,x)
        max_x = 307.65;
        const num_type log10_2_hi = 0.30102999554947019;     // log10(2) in two parts
        const num_type log10_2_lo = 1.1451100899212592E-10;
        x  = initial_x;
        r  = round(initial_x*(num_type(VM_LOG2E)*num_type(VM_LN10)));
        // subtraction in two steps for higher precision
        x  = nmul_add(r, log10_2_hi, x);         //  x -= r * log10_2_hi;
        x  = nmul_add(r, log10_2_lo, x);         //  x -= r * log10_2_lo;
        x  = x*num_type(VM_LN10);
    }
    else  {  // undefined value of BA
        return vec_type(0.);
    }

    z = _vcl::polynomial_13m(x, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13);

    if (BA == 1) r--;  // 0.5 * exp(x)

    // multiply by power of 2 
    n2 = vm_pow2n(r);

    if (M1 == 0) {
        // expOpCounter::impl::to_original
        z = (z + num_type(1.0)) * n2;
    }
    else {
        // expm1
        z = mul_add(z, n2, n2 - num_type(1.0));            // z = z * n2 + (n2 - 1.0);
    }
    
    // check for overflow
    inrange  = abs(initial_x) < max_x;
    // check for INF and NAvcl::exp2N
    inrange &= _vcl::is_finite(OpCounter::impl::to_original(initial_x));

    if (horizontal_and(inrange)) {
        // fast normal path
        return z;
    }
    else {
        /*
        // overflow, underflow and NAN
        r = select(_vcl::sign_bit(
            OpCounter::impl::to_original(initial_x), 0.-M1,
            OpCounter::impl::to_original(_vcl::infinite_vec<VTYPE>()))); // value in case of +/- overflow or INF
        z = select(inrange, z, r);                                     // +/- underflow
        z = select(_vcl::is_nan(OpCounter::impl::to_original(initial_x)),
                   initial_x, z);                   // NAN goes through
        return z;
        */
    }
    
    return z;
}

int main (int argc, char *argv[])
{
    std::cout << "Counting operations for a exponential vectorclass operation."
        << std::endl;
        
    vec_type vec = opc_experiment_exp_d<vec_type, bool_vec_type, 0, 2>(
        vec_type(-100, 0, 0, 0));
    std::cout << vec[0] << " " << vec[1] << " " << vec[2] << " " << vec[3] << std::endl;
    
    OpCounter::OpCounter<double>::counters.reportOperations(std::cout, "");
}
