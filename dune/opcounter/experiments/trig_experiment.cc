#include <dune/opcounter/opcounter.hh>
#include <dune/opcounter/vectorclass.hh>
#include <vectorclass/vectorclass.h>
#include <vectorclass/vectormath_common.h>
#include <vectorclass/vectormath_trig.h>
#include <iostream>

#define VM_PI       3.14159265358979323846           // pi

using num_type = OpCounter::OpCounter<double>;
using vec_type = OpCounter::impl::OpCounterVector<double, 4>;
using bool_vec_type = _vcl::Vec4db;

/* These functions have been copied from the vectorclass code and modified to
 * function with OpCounters. The aliases above might not immediately work for
 * every type. The modifications are not exactly clean.
 */

template<>
inline vec_type _vcl::vm_half_int_vector_to_double<vec_type, _vcl::Vec4i>(_vcl::Vec4i const & x) 
{
    return OpCounter::impl::to_opcounted<double, 4>(_vcl::to_double(x));
}

// *************************************************************
//             sincos template, double precision
// *************************************************************
// Template parameters:
// VTYPE:  f.p. vector type
// ITYPE:  integer vector type with same element size
// ITYPEH: integer vector type with half the element size
// BVTYPE: boolean vector type
// SC:     1 = sin, 2 = cos, 3 = sincos
// Paramterers:
// xx = input x (radians)
// cosret = return pointer (only if SC = 3)
template<class VTYPE, class ITYPE, class ITYPEH, class BVTYPE, int SC>
static inline VTYPE opc_experiment_sincos_d(VTYPE * cosret, VTYPE const & xx) {

    // define constants
    const num_type ONEOPIO4 = 4. / VM_PI;

    const num_type P0sin = -1.66666666666666307295E-1;
    const num_type P1sin = 8.33333333332211858878E-3;
    const num_type P2sin = -1.98412698295895385996E-4;
    const num_type P3sin = 2.75573136213857245213E-6;
    const num_type P4sin = -2.50507477628578072866E-8;
    const num_type P5sin = 1.58962301576546568060E-10;

    const num_type P0cos = 4.16666666666665929218E-2;
    const num_type P1cos = -1.38888888888730564116E-3;
    const num_type P2cos = 2.48015872888517045348E-5;
    const num_type P3cos = -2.75573141792967388112E-7;
    const num_type P4cos = 2.08757008419747316778E-9;
    const num_type P5cos = -1.13585365213876817300E-11;

    const num_type DP1 = 7.853981554508209228515625E-1;
    const num_type DP2 = 7.94662735614792836714E-9;
    const num_type DP3 = 3.06161699786838294307E-17;
    /*
    const num_type DP1sc = 7.85398125648498535156E-1;
    const num_type DP2sc = 3.77489470793079817668E-8;
    const num_type DP3sc = 2.69515142907905952645E-15;
    */
    VTYPE  xa, x, y, x2, s, c, sin1, cos1;       // data vectors
    ITYPEH q;                                    // integer vectors, 32 bit
    ITYPE  qq, signsin, signcos;                 // integer vectors, 64 bit
    BVTYPE swap, overflow;                       // boolean vectors

    xa = abs(xx);

    // Find quadrant
    //      0 -   pi/4 => 0
    //   pi/4 - 3*pi/4 => 2
    // 3*pi/4 - 5*pi/4 => 4
    // 5*pi/4 - 7*pi/4 => 6
    // 7*pi/4 - 8*pi/4 => 8

    // truncate to integer (magic number conversion is not faster here)
    q = vm_truncate_low_to_int(OpCounter::impl::to_original(xa * ONEOPIO4));
    q = (q + 1) & ~1;

    y = _vcl::vm_half_int_vector_to_double<VTYPE>(q);  // quadrant, as double

    // Reduce by extended precision modular arithmetic
    x = nmul_add(y, DP3, nmul_add(y, DP2, nmul_add(y, DP1, xa)));    
        // x = ((xa - y * DP1) - y * DP2) - y * DP3;

    // Expansion of sin and cos, valid for -pi/4 <= x <= pi/4
    x2 = x * x;
    s = _vcl::polynomial_5(x2, P0sin, P1sin, P2sin, P3sin, P4sin, P5sin);
    c = _vcl::polynomial_5(x2, P0cos, P1cos, P2cos, P3cos, P4cos, P5cos);
    s = mul_add(x * x2, s, x);                                       // s = x + (x * x2) * s;
    c = mul_add(x2 * x2, c, nmul_add(x2, OpCounter::OpCounter<double>(0.5), 
                                     OpCounter::OpCounter<double>(1.0)));                 
    // c = 1.0 - x2 * 0.5 + (x2 * x2) * c;

    // correct for quadrant
    qq = _vcl::vm_half_int_vector_to_full<ITYPE, ITYPEH>(q);
    swap = BVTYPE((qq & 2) != 0);

    // check for overflow
    if (horizontal_or(q < 0)) {
        overflow = (y < OpCounter::OpCounter<double>(0)) & 
            _vcl::is_finite(OpCounter::impl::to_original(xa));
        s = select(overflow, OpCounter::OpCounter<double>(0.), s);
        c = select(overflow, OpCounter::OpCounter<double>(1.), c);
    }

    if (SC & 1) {  // calculate sin
        sin1 = select(swap, c, s);
        signsin = ((qq << 61) ^ ITYPE(reinterpret_i(
            OpCounter::impl::to_original(xx)))) & ITYPE(1ULL << 63);
        //sin1 ^= reinterpret_d(signsin);
    }
    if (SC & 2) {  // calculate cos
        cos1 = select(swap, s, c);
        signcos = ((qq + 2) << 61) & (1ULL << 63);
        //cos1 ^= reinterpret_d(signcos);
    }
    if (SC == 3) {  // calculate both. cos returned through pointer
        *cosret = cos1;
    }
    if (SC & 1) return sin1; else return cos1;
}

int main (int argc, char *argv[])
{
    std::cout << "Counting operations for a trigonometric vectorclass operation."
        << std::endl;
    
    vec_type vec = opc_experiment_sincos_d<
        vec_type, _vcl::Vec4q, _vcl::Vec4i, _vcl::Vec4db, 2>(0, vec_type(100, 0, 0, 0));
    std::cout << vec[0] << " " << vec[1] << " " << vec[2] << " " << vec[3] << std::endl;
    
    OpCounter::OpCounter<double>::counters.reportOperations(std::cout, "");
}
