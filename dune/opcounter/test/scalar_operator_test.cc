#include <Catch2/single_include/catch.hpp>
#include <dune/opcounter/opcounter.hh>
#include "test_impl.hh"

TEST_CASE("Testing arithmetic operators of scalar OpCounters (double)")
{
    // This array should have values that produce meaningful tests
    std::vector<double> test_values(10);
    test_values[0] = 0;
    test_values[1] = 2;
    test_values[2] = -1;
    test_values[3] = 3.1415926535897;
    test_values[4] = 2.718281828459045;
    test_values[5] = 9999.9999;
    test_values[6] = -1000.5;
    test_values[7] = 0.1e-308;
    test_values[8] = -1.7e308;
    test_values[9] = 1.7e308;
    
    // Operators to test:
    // +, -, *, /, <, <=, >, >=, !=, ==, +=
	test_impl_binary_num<double>("double", "+",  test_values, 
                             [](auto a, auto b){return a + b;});
	test_impl_binary_num<double>("double", "-",  test_values, 
                             [](auto a, auto b){return a - b;});
	test_impl_binary_num<double>("double", "*",  test_values, 
                             [](auto a, auto b){return a * b;});
	test_impl_binary_num<double>("double", "/",  test_values, 
                             [](auto a, auto b){return a / b;});
	test_impl_binary_com<double>("double", "<",  test_values, 
                             [](auto a, auto b){return a < b;});
	test_impl_binary_com<double>("double", "<=", test_values, 
                             [](auto a, auto b){return a <= b;});
	test_impl_binary_com<double>("double", ">",  test_values, 
                             [](auto a, auto b){return a > b;});
	test_impl_binary_com<double>("double", ">=", test_values, 
                             [](auto a, auto b){return a >= b;});
	test_impl_binary_com<double>("double", "!=", test_values, 
                             [](auto a, auto b){return a != b;});
	test_impl_binary_com<double>("double", "==", test_values, 
                             [](auto a, auto b){return a == b;});
	test_impl_unary<double>("double", "+=", test_values, 
                             [](auto a){return a += 10 ;});
}

TEST_CASE("Testing arithmetic operators of scalar OpCounters (float)")
{
    std::vector<float> test_values(10);
    test_values[0] = 0;
    test_values[1] = 2;
    test_values[2] = -1;
    test_values[3] = 3.1415926535897;
    test_values[4] = 2.718281828459045;
    test_values[5] = 9999.9999;
    test_values[6] = -1000.5;
    test_values[7] = 0.1e-38;
    test_values[8] = -1.7e38;
    test_values[9] = 1.7e38;
    
	test_impl_binary_num<float>("float", "+",  test_values, 
                            [](auto a, auto b){return a + b;});
	test_impl_binary_num<float>("float", "-",  test_values, 
                            [](auto a, auto b){return a - b;});
	test_impl_binary_num<float>("float", "*",  test_values, 
                            [](auto a, auto b){return a * b;});
	test_impl_binary_num<float>("float", "/",  test_values, 
                            [](auto a, auto b){return a / b;});
	test_impl_binary_com<float>("float", "<",  test_values, 
                            [](auto a, auto b){return a < b;});
	test_impl_binary_com<float>("float", "<=", test_values, 
                            [](auto a, auto b){return a <= b;});
	test_impl_binary_com<float>("float", ">",  test_values, 
                            [](auto a, auto b){return a > b;});
	test_impl_binary_com<float>("float", ">=", test_values, 
                            [](auto a, auto b){return a >= b;});
	test_impl_binary_com<float>("float", "!=", test_values, 
                            [](auto a, auto b){return a != b;});
	test_impl_binary_com<float>("float", "==", test_values, 
                            [](auto a, auto b){return a == b;});
	test_impl_unary<float>("float", "+=", test_values, 
                             [](auto a){return a += 10 ;});
}

TEST_CASE("Testing arithmetic operators of scalar OpCounters (anything arithmetic: int)")
{
    // This array should have values that produce meaningful tests
    std::vector<int> test_values(5);
    test_values[0] = 0;
    test_values[1] = 2;
    test_values[2] = -1;
    test_values[3] = 1000;
    test_values[4] = 2147483647;
    
    // Operators to test:
    // +, -, *, /, <, <=, >, >=, !=, ==, +=
	test_impl_binary_num<int>("int", "+",  test_values, 
                             [](auto a, auto b){return a + b;});
	test_impl_binary_num<int>("int", "-",  test_values, 
                             [](auto a, auto b){return a - b;});
	test_impl_binary_num<int>("int", "*",  test_values, 
                             [](auto a, auto b){return a * b;});
	test_impl_binary_num<int>("int", "/",  test_values, 
                             [](auto a, auto b){return a / b;});
	test_impl_binary_com<int>("int", "<",  test_values, 
                             [](auto a, auto b){return a < b;});
	test_impl_binary_com<int>("int", "<=", test_values, 
                             [](auto a, auto b){return a <= b;});
	test_impl_binary_com<int>("int", ">",  test_values, 
                             [](auto a, auto b){return a > b;});
	test_impl_binary_com<int>("int", ">=", test_values, 
                             [](auto a, auto b){return a >= b;});
	test_impl_binary_com<int>("int", "!=", test_values, 
                             [](auto a, auto b){return a != b;});
	test_impl_binary_com<int>("int", "==", test_values, 
                             [](auto a, auto b){return a == b;});
	test_impl_unary<int>("int", "+=", test_values, 
                             [](auto a){return a += 10 ;});
}
