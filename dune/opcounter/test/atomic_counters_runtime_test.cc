#include <dune/opcounter/opcounter.hh>
#include <chrono>
#include <iostream>

/* This test is built to compare execution time of an algorithm (Newton method)
 * when the OpCounter increments are atomic versus when they are not.
 * I have programmed a macro into dune/opcounter/scalar/definition.hh that can
 * switch the operations to be atomic or not and this test needs to be
 * executed with switching it in between.
 */

/** Simple Newton method for f(x) = x² - 1
 * \param initial_guess starting value x0 for the newton method
 * \param iterations amount of iterations for the method
 */
template<typename T>
T newton_method (T input, T initial_guess, int iterations)
{
    T curr = initial_guess;
    
    for( int i = 0; i < iterations; i++)
    {
        T prev = curr;
        curr = prev - (prev*prev - input) / (2 * prev);
    }
    
    return curr;
}

int main (int argc, char *argv[])
{
    auto start_time = std::chrono::high_resolution_clock::now();
    
    OpCounter::OpCounter<double> result;
    for( int i = 0; i < 1000; i++)
    {
        result = newton_method(OpCounter::OpCounter<double>(123456789.0), 
                               OpCounter::OpCounter<double>(1.0), 1000000);
    }
    
    auto end_time = std::chrono::high_resolution_clock::now();
    
    std::chrono::duration<double> elapsed_seconds = end_time - start_time;
    
    std::cout << "The Newton method took " << elapsed_seconds.count() << " seconds."
        << std::endl;
        
    std::cout << result << std::endl;
}
