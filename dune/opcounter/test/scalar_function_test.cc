#include <Catch2/single_include/catch.hpp>
#include <dune/opcounter/opcounter.hh>
#include "test_impl.hh"

TEST_CASE("Testing arithmetic functions of scalar OpCounters (double)")
{
    std::vector<double> test_values(10);
    test_values[0] = 0;
    test_values[1] = 2;
    test_values[2] = -1;
    test_values[3] = 3.1415926535897;
    test_values[4] = 2.718281828459045;
    test_values[5] = 9999.9999;
    test_values[6] = -1000.5;
    test_values[7] = 0.1e-308;
    test_values[8] = -1.7e308;
    test_values[9] = 1.7e308;
    
    // Function to test:
    // exp, sin, cos, sqrt, abs, max, min, round, trunc, ceil, floor, pow, isnan
	test_impl_unary<double>("double", "exp",  test_values, 
                            [](auto a){using std::exp; return exp(a);});
	test_impl_unary<double>("double", "sin",  test_values, 
                            [](auto a){using std::sin; return sin(a);});
	test_impl_unary<double>("double", "cos",  test_values, 
                            [](auto a){using std::cos; return cos(a);});
	test_impl_unary<double>("double", "sqrt",  test_values, 
                            [](auto a){using std::sqrt; return sqrt(a);});
	test_impl_unary<double>("double", "abs",  test_values, 
                            [](auto a){using std::abs; return abs(a);});
	test_impl_unary<double>("double", "round",  test_values, 
                            [](auto a){using std::round; return round(a);});
	test_impl_unary<double>("double", "trunc",  test_values, 
                            [](auto a){using std::trunc; return trunc(a);});
	test_impl_unary<double>("double", "ceil",  test_values, 
                            [](auto a){using std::ceil; return ceil(a);});
	test_impl_unary<double>("double", "floor",  test_values, 
                            [](auto a){using std::floor; return floor(a);});
    test_impl_binary_only_opc_com<double>("double", "max",  test_values, 
                            [](auto a, auto b){using std::max; return max(a,b);});
	test_impl_binary_only_opc_com<double>("double", "min",  test_values, 
                            [](auto a, auto b){using std::min; return min(a,b);});
	test_impl_binary_only_opc_num<double>("double", "pow",  test_values, 
                            [](auto a, auto b){using std::pow; return pow(a,b);});
}

TEST_CASE("Testing arithmetic functions of scalar OpCounters (float)")
{
    std::vector<float> test_values(10);
    test_values[0] = 0;
    test_values[1] = 2;
    test_values[2] = -1;
    test_values[3] = 3.1415926535897;
    test_values[4] = 2.718281828459045;
    test_values[5] = 9999.9999;
    test_values[6] = -1000.5;
    test_values[7] = 0.1e-38;
    test_values[8] = -1.7e38;
    test_values[9] = 1.7e38;
    
    // Function to test:
    // exp, sin, cos, sqrt, abs, max, min, round, trunc, ceil, floor, pow, isnan
	test_impl_unary<float>("float", "exp",  test_values, 
                            [](auto a){using std::exp; return exp(a);});
	test_impl_unary<float>("float", "sin",  test_values, 
                            [](auto a){using std::sin; return sin(a);});
	test_impl_unary<float>("float", "cos",  test_values, 
                            [](auto a){using std::cos; return cos(a);});
	test_impl_unary<float>("float", "sqrt",  test_values, 
                            [](auto a){using std::sqrt; return sqrt(a);});
	test_impl_unary<float>("float", "abs",  test_values, 
                            [](auto a){using std::abs; return abs(a);});
	test_impl_unary<float>("float", "round",  test_values, 
                            [](auto a){using std::round; return round(a);});
	test_impl_unary<float>("float", "trunc",  test_values, 
                            [](auto a){using std::trunc; return trunc(a);});
	test_impl_unary<float>("float", "ceil",  test_values, 
                            [](auto a){using std::ceil; return ceil(a);});
	test_impl_unary<float>("float", "floor",  test_values, 
                            [](auto a){using std::floor; return floor(a);});
    test_impl_binary_only_opc_com<float>("float", "max",  test_values, 
                            [](auto a, auto b){using std::max; return max(a,b);});
	test_impl_binary_only_opc_com<float>("float", "min",  test_values, 
                            [](auto a, auto b){using std::min; return min(a,b);});
	test_impl_binary_only_opc_num<float>("float", "pow",  test_values, 
                            [](auto a, auto b){using std::pow; return pow(a,b);});
}
    
