#ifndef DUNE_OPCOUNTER_VECTOR_TRAITS_HH
#define DUNE_OPCOUNTER_VECTOR_TRAITS_HH


namespace OpCounter {
  namespace impl {

    /* A struct that maps (F, size) to the VCL boolean vector for that pair */
    template<typename F, int size>
    struct BooleanVector
    {};

    template<>
    struct BooleanVector<double, 2>
    {
      using type = VCL_NAMESPACE::Vec2db;
    };

    template<>
    struct BooleanVector<float, 4>
    {
      using type = VCL_NAMESPACE::Vec4fb;
    };

#if MAX_VECTOR_SIZE >= 256
    template<>
    struct BooleanVector<double, 4>
    {
      using type = VCL_NAMESPACE::Vec4db;
    };

    template<>
    struct BooleanVector<float, 8>
    {
      using type = VCL_NAMESPACE::Vec8fb;
    };
#endif

#if MAX_VECTOR_SIZE >= 512
    template<>
    struct BooleanVector<double, 8>
    {
      using type = VCL_NAMESPACE::Vec8db;
    };

    template<>
    struct BooleanVector<float, 16>
    {
      using type = VCL_NAMESPACE::Vec16fb;
    };
#endif

    /* A struct that maps (F, size) to the original VCL vector for that pair */
    template<typename F, int size>
    struct OriginalVector
    {};

    template<>
    struct OriginalVector<double, 2>
    {
      using type = VCL_NAMESPACE::Vec2d;
    };

    template<>
    struct OriginalVector<float, 4>
    {
      using type = VCL_NAMESPACE::Vec4f;
    };

#if MAX_VECTOR_SIZE >= 256
    template<>
    struct OriginalVector<double, 4>
    {
      using type = VCL_NAMESPACE::Vec4d;
    };

    template<>
    struct OriginalVector<float, 8>
    {
      using type = VCL_NAMESPACE::Vec8f;
    };
#endif

#if MAX_VECTOR_SIZE >= 512
    template<>
    struct OriginalVector<double, 8>
    {
      using type = VCL_NAMESPACE::Vec8d;
    };

    template<>
    struct OriginalVector<float, 16>
    {
      using type = VCL_NAMESPACE::Vec16f;
    };
#endif

  } // namespace impl
} // namespace OpCounter

#endif
