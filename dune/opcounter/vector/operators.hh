#ifndef DUNE_OPCOUNTER_VECTOR_OPERATORS_HH
#define DUNE_OPCOUNTER_VECTOR_OPERATORS_HH

#include<dune/opcounter/vector/definition.hh>
#include<dune/opcounter/vector/tools.hh>

#define DEFINE_OPCOUNT_VECTOR_ARITHMETIC_OPERATOR(OP)                             \
  /* left: opcounted vector, right: opcounted vector */                           \
  template<typename F, int size>                                                  \
  OpCounterVector<F, size> operator OP(                                           \
      const OpCounterVector<F, size>& a,                                          \
      const OpCounterVector<F, size>& b)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    std::transform(a._v, a._v + size, b._v, r._v,                                 \
                   [](auto x, auto y){ return x OP y;});                          \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  /* left: opcounter, right: opcounted vector */                                  \
  template<typename F, int size>                                                  \
  OpCounterVector<F, size> operator OP(                                           \
      const OpCounter<F>& a,                                                      \
      const OpCounterVector<F, size>& b)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    std::transform(b._v, b._v + size, r._v,                                       \
                   [a](auto x){ return a OP x;});                                 \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  /* left: opcounted vector, right: opcounter */                                  \
  template<typename F, int size>                                                  \
  OpCounterVector<F, size> operator OP(                                           \
      const OpCounterVector<F, size>& a,                                          \
      const OpCounter<F>& b)                                                      \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> r;                                                   \
    std::transform(a._v, a._v + size, r._v,                                       \
                   [b](auto x){ return x OP b;});                                 \
    BARRIER;                                                                      \
    return r;                                                                     \
  }                                                                               \
                                                                                  \
  /* accumulating arithmetic operator */                                          \
  template<typename F, int size>                                                  \
  OpCounterVector<F, size>& operator OP##=(                                       \
      OpCounterVector<F, size>& a,                                                \
      const OpCounterVector<F, size>& b)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    std::transform(a._v, a._v + size, b._v, a._v,                                 \
    		   [](auto x, auto y){ return x OP y;});                          \
    BARRIER;                                                                      \
    return a;                                                                     \
  }                                                                               \


#define DEFINE_OPCOUNT_VECTOR_LOGICAL_OPERATOR(OP)                                \
  template<typename F, int size>                                                  \
  typename BooleanVector<F, size>::type operator OP(                              \
      const OpCounterVector<F, size>& a,                                          \
      const OpCounterVector<F, size>& b)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounter<F>::comparisons(size);                                              \
    auto a_ = to_original(a);                                                     \
    auto b_ = to_original(b);                                                     \
    return a_ OP b_;                                                              \
  }                                                                               \
                                                                                  \
  /* left operand: vector, right operand: opcounted scalar */                     \
  template<typename T, typename F, int size,                                      \
      typename std::enable_if<std::is_convertible<T, F>::value, int>::type = 0>   \
  typename BooleanVector<F, size>::type operator OP(                              \
      const OpCounterVector<F, size>& a,                                          \
	  const OpCounter<T>& b)                                                      \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounter<F>::comparisons(size);                                              \
    auto a_ = to_original(a);                                                     \
    return a_ OP b._v;                                                            \
  }                                                                               \
                                                                                  \
  /* left operand: opcounted scalar, right operand: vector */                     \
  template<typename T, typename F, int size,                                      \
      typename std::enable_if<std::is_convertible<T, F>::value, int>::type = 0>   \
  typename BooleanVector<F, size>::type operator OP(                              \
      const OpCounter<T>& a,                                                      \
	  const OpCounterVector<F, size>& b)                                          \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounter<F>::comparisons(size);                                              \
    auto b_ = to_original(b);                                                     \
    return a._v OP b_;                                                            \
  }                                                                               \


#define DEFINE_OPCOUNT_VECTOR_INCREMENT_OPERATOR(OP)                              \
  /* prefix operator */                                                           \
  template<typename F, int size>                                                  \
  OpCounterVector<F, size>& operator OP##OP(                                      \
      OpCounterVector<F, size>& a)                                                \
  {                                                                               \
    BARRIER;                                                                      \
    a = a + OpCounterVector<F, size>(1.0);                                        \
    BARRIER;                                                                      \
    return a;                                                                     \
  }                                                                               \
                                                                                  \
  /* postfix operator */                                                          \
  template<typename F, int size>                                                  \
  OpCounterVector<F, size> operator OP##OP(                                       \
      OpCounterVector<F, size>& a, int)                                           \
  {                                                                               \
    BARRIER;                                                                      \
    OpCounterVector<F, size> a0(a);                                               \
    a = a + OpCounterVector<F, size>(1.0);                                        \
    BARRIER;                                                                      \
    return a0;                                                                    \
  }                                                                               \


namespace OpCounter {
  namespace impl {

    /* arithmetic operations */
    DEFINE_OPCOUNT_VECTOR_ARITHMETIC_OPERATOR(+);
    DEFINE_OPCOUNT_VECTOR_ARITHMETIC_OPERATOR(-);
    DEFINE_OPCOUNT_VECTOR_ARITHMETIC_OPERATOR(*);
    DEFINE_OPCOUNT_VECTOR_ARITHMETIC_OPERATOR(/);

    /* logical operators */
    DEFINE_OPCOUNT_VECTOR_LOGICAL_OPERATOR(<);
    DEFINE_OPCOUNT_VECTOR_LOGICAL_OPERATOR(<=);
    DEFINE_OPCOUNT_VECTOR_LOGICAL_OPERATOR(>);
    DEFINE_OPCOUNT_VECTOR_LOGICAL_OPERATOR(>=);
    DEFINE_OPCOUNT_VECTOR_LOGICAL_OPERATOR(!=);
    DEFINE_OPCOUNT_VECTOR_LOGICAL_OPERATOR(==);

    /* increment operators */
    DEFINE_OPCOUNT_VECTOR_INCREMENT_OPERATOR(+);
    DEFINE_OPCOUNT_VECTOR_INCREMENT_OPERATOR(-);

  } // namespace impl
} // namespace OpCounter

#endif
