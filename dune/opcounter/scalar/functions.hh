#ifndef DUNE_OPCOUNTER_SCALAR_FUNCTIONS_HH
#define DUNE_OPCOUNTER_SCALAR_FUNCTIONS_HH

#include<cmath>

#define DEFINE_OPCOUNT_STD_FUNCTION(FUNC)                         \
  template<typename F>                                            \
  OpCounter<F> FUNC(const OpCounter<F>& x)                        \
  {                                                               \
    using std::FUNC;                                              \
    return {std::FUNC(x._v)};                                     \
  }

#define DEFINE_OPCOUNT_STD_FUNCTION_COUNTED(FUNC, COUNTER)        \
  template<typename F>                                            \
  OpCounter<F> FUNC(const OpCounter<F>& x)                        \
  {                                                               \
    ++OpCounter<F>::counters.COUNTER##_count;                     \
    using std::FUNC;                                              \
    return {std::FUNC(x._v)};                                     \
  }


namespace OpCounter {

  // Define functions from the standard library, whose occurences
  // we would like to count.
  DEFINE_OPCOUNT_STD_FUNCTION_COUNTED(exp, exp);
  DEFINE_OPCOUNT_STD_FUNCTION_COUNTED(sin, sin);
  DEFINE_OPCOUNT_STD_FUNCTION_COUNTED(cos, sin);
  DEFINE_OPCOUNT_STD_FUNCTION_COUNTED(sqrt, sqrt);
  DEFINE_OPCOUNT_STD_FUNCTION_COUNTED(abs, comparison);
  DEFINE_OPCOUNT_STD_FUNCTION_COUNTED(acos, sin);
  DEFINE_OPCOUNT_STD_FUNCTION_COUNTED(tanh, exp);

  // Define functions from the standard library, that we need to provide
  // a template specialization of, but we do not care about their counts.
  DEFINE_OPCOUNT_STD_FUNCTION(round);
  DEFINE_OPCOUNT_STD_FUNCTION(trunc);
  DEFINE_OPCOUNT_STD_FUNCTION(ceil);
  DEFINE_OPCOUNT_STD_FUNCTION(floor);

  // The pow function is the only two-operand function so far. We do not
  // write a macro until we hit another use case.
  template<typename F>
  OpCounter<F> pow(const OpCounter<F>& a, const OpCounter<F>& b)
  {
    ++OpCounter<F>::counters.pow_count;
    using std::pow;
    return {pow(a._v,b._v)};
  }

  template<typename F>
  OpCounter<F> max(const OpCounter<F>& a, const OpCounter<F>& b)
  {
    ++OpCounter<F>::counters.comparison_count;
    using std::max;
    return {max(a._v,b._v)};
  }

  template<typename F>
  OpCounter<F> min(const OpCounter<F>& a, const OpCounter<F>& b)
  {
    ++OpCounter<F>::counters.comparison_count;
    using std::min;
    return {min(a._v,b._v)};
  }

  // isnan is the only bool returning function so far. One could of course
  // build the return type into above macro...
  template<typename F>
  bool isnan(const OpCounter<F>& a)
  {
    using std::isnan;
    return isnan(a._v);
  }

  template<typename F>
  OpCounter<F> frexp(const OpCounter<F>& a, int* b)
  {
    using std::frexp;
    return {frexp(a._v,b)};
  }

  template<typename F>
  long lround(const OpCounter<F>& a)
  {
    using std::lround;
    return lround(a._v);
  }

} // namespace OpCounter


#endif
